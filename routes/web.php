<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('home');

Route::get('/users/index', [UserController::class, 'index'])->name('users.index');
Route::get('/users/create', '\App\Http\Controllers\UserController@create')->name('users.create');
Route::post('/users/store', '\App\Http\Controllers\UserController@store')->name('users.store');
Route::get('/users/edit/{id}', '\App\Http\Controllers\UserController@edit')->name('users.edit');
Route::post('/users/update/{id}', '\App\Http\Controllers\UserController@update')->name('users.update');
Route::post('/users/delete/{id}', '\App\Http\Controllers\UserController@delete')->name('users.delete');

Route::get('/roles/index', [RoleController::class, 'index'])->name('roles.index');
Route::get('/roles/create', '\App\Http\Controllers\RoleController@create')->name('roles.create');
Route::post('/roles/store', '\App\Http\Controllers\RoleController@store')->name('roles.store');
Route::get('/roles/edit/{id}', '\App\Http\Controllers\RoleController@edit')->name('roles.edit');
Route::post('/roles/update/{id}', '\App\Http\Controllers\RoleController@update')->name('roles.update');
Route::post('/roles/delete/{id}', '\App\Http\Controllers\RoleController@delete')->name('roles.delete');

Route::get('/login', [AuthController::class, 'viewLogin'])->name('login');
Route::post('/login', [AuthController::class, 'loginForm'])->name('submitLogin');
Route::get('/logout', [AuthController::class, 'logout'])->name('logout');
