<?php

namespace App\Services;

use App\Repositories\UserRepository;
use App\Repositories\UserRoleRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Webpatser\Uuid\Uuid;

class UserRoleService extends BaseService
{
   public $userRepo;
   public $userRoleRepo;
   public function __construct(UserRepository $UserRepository, UserRoleRepository $UserRoleRepo)
   {
      $this->userRepo = $UserRepository;
      $this->userRoleRepo = $UserRoleRepo;
   }

   public function get()
   {
      try {
         $userRepo = $this->userRepo->get();
         if ($userRepo) {
            return $userRepo;
         }
      } catch (\Exception $e) {
         Log::error($e->getMessage());
         return $this->sendError(
            ['error']
         );
      }
   }

   public function create($params)
   {
      $userRepo = $this->userRepo->store($params);
      try {
         if ($userRepo) {
            $email = new \App\Jobs\SendWelcomeEmail([
               'name' => $userRepo->name,
               'email' => $userRepo->email,
           ]);
            dispatch($email);
            $data = [
               'user_id' => $userRepo->id,
               'role_id' => $params['code'],
            ];
            $this->userRoleRepo->store($data);
         }
      } catch (\Exception $e) {
         Log::error($e->getMessage());
         return $this->sendError(
            ['error']
         );
      }
   }

   // public function detail($id){
   //    $userRepo = $this->userRepo->getById($id);
   // }
}
