<?php

namespace App\Services;

use App\Repositories\PermissionRepository;
use App\Repositories\PermissionRoleRepository;
use App\Repositories\RoleRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Webpatser\Uuid\Uuid;

class RoleService extends BaseService
{
   public $roleRepo;
   public $permissionRepo;
   public $permissionRoleRepo;
   public function __construct(RoleRepository $RoleRepository, PermissionRepository $PermissionRepository, PermissionRoleRepository $PermissionRoleRepository)
   {
      $this->roleRepo = $RoleRepository;
      $this->permissionRepo = $PermissionRepository;
      $this->permissionRoleRepo = $PermissionRoleRepository;
   }

   public function get()
   {
      try {
         $roleRepo = $this->roleRepo->get();
         if ($roleRepo) {
            return $roleRepo;
         }
      } catch (\Exception $e) {
         Log::error($e->getMessage());
         return $this->sendError(
            [trans('message.NOT_COMPLETE')]
         );
      }
   }

   public function create($params)
   {
      try {
         DB::beginTransaction();
         $roleRepo = $this->roleRepo->store($params);
         if ($roleRepo && !empty($params['permission'])) {
            $roleRepo->permissions()->attach($params['permission']);
         }
         DB::commit();
      } catch (\Exception $e) {
         DB::rollBack();
         Log::error($e->getMessage());
         return $this->sendError(
            ['error']
         );
      }
   }

   public function update($id, $params)
   {
      try {
         DB::beginTransaction();
         $this->roleRepo->updateById($id, [
            'code' => $params['code'],
         ]);
         $dataPer = [];
         $dataPermissionRole = $this->permissionRoleRepo->getPermissionRole($id);
         $permissionRoleIds = $dataPermissionRole->pluck('permission_id')->all();
         if (!empty($params['permission'])) {
            foreach ($params['permission'] as $per) {
               if (!in_array($per, $permissionRoleIds)) {
                  $dataPer[] = [
                     'role_id' => $id,
                     'permission_id' => $per
                  ];
               } else {
                  $roleDetail = $this->roleRepo->getById($id);
                  $roleDetail->permissions()->attach($params['permission']);
               }
            }
            if (!empty($dataPer)) {
               $this->permissionRoleRepo->insertMultiRecord($dataPer);
            }
            foreach ($dataPermissionRole as $item) {
               if (!in_array($item->permission_id, $params['permission'])) {
                  $item->delete();
               }
            }
         } else {
            foreach ($dataPermissionRole as $item) {
               $item->delete();
            }
         }
         DB::commit();
      } catch (\Exception $e) {
         DB::rollBack();
         Log::error($e->getMessage());
         return $this->sendError(
            ['error']
         );
      }
   }

   public function delete($id)
   {
      try {
         DB::beginTransaction();
         $role = $this->roleRepo->getById($id);
         $role->delete();
         $role->permissions()->detach();
      } catch (\Exception $e) {
         DB::rollBack();
         Log::error($e->getMessage());
         return $this->sendError(
            ['error']
         );
      }
   }
}
