<?php

namespace App\Services;

use App\Repositories\PermissionRepository;
use App\Repositories\RoleRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class PermisionService extends BaseService
{
   public $permissionRepo;
   public function __construct(PermissionRepository $PermissionRepository)
   {
    $this->permissionRepo = $PermissionRepository;
   }
   
   public function all(){
      try {
         $permissionRepo = $this->permissionRepo->all();
         if($permissionRepo){
            return $permissionRepo;
         }
     } catch (\Exception $e) {
         Log::error($e->getMessage());
         return $this->sendError(
             [trans('message.NOT_COMPLETE')]
         );
     }
   }
}
