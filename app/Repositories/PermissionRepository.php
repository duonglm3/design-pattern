<?php

namespace App\Repositories;

use App\Models\Permission;
use App\Models\Role;

class PermissionRepository extends Repository
{
    public function __construct()
    {
        parent::__construct(Permission::class);
        $this->fields = Permission::FIELDS;
    }

    

}
