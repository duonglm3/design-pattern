<?php

namespace App\Repositories;

use App\Models\PermissionRole;
use App\Models\Role;
use App\Models\UserRole;

class PermissionRoleRepository extends Repository
{
    public function __construct()
    {
        parent::__construct(PermissionRole::class);
        $this->fields = PermissionRole::FIELDS;
    }

    public function permissionIds($id){
        return $this->getModel()::where('role_id', $id)->pluck('permission_id')->all();
    }

    public function getPermissionRole($id){
        return $this->getModel()::where('role_id', $id)->get();
    }

    // public function update($id, $data)
    // {
    //     $data = UserRole::where('user_id', $id)->update($data);
    //     return $data;
    // }
}
