<?php

namespace App\Repositories;
use App\Models\Role;
use App\Models\UserRole;

class UserRoleRepository extends Repository
{
    public function __construct()
    {
        parent::__construct(UserRole::class);
        $this->fields = UserRole::FIELDS;
    }

    public function rolIds($id){
        return $this->getModel()::where('user_id', $id)->pluck('role_id')->all();
    }

    public function update($id, $data)
    {
        $data = UserRole::where('user_id', $id)->update($data);
        return $data;
    }
}
