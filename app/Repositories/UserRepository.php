<?php

namespace App\Repositories;

use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UserRepository extends Repository
{
    public function __construct()
    {
        parent::__construct(User::class);
        $this->fields = $this->getInstance()->getFillable();
    }

    public function delete($id){
        return $this->getModel()::where('user_id', $id)->delete();
    }

    public function getUserByUserId(string $userId)
    {
        return $this->getInstance()->where('user_id', $userId)->first();
    }

    public function formatAllRecord($records)
    {
        if (!empty($records)) {
            foreach ($records as &$record) {
                $record = $this->formatRecord($record);
            }
        }
        return $records;
    }

    public function formatRecord($record)
    {
        //$record->name = $record
        return $record;
    }

    public function updateById($id, array $data)
    {
        return $this->getModel()::where('id', $id)->update($data);
    }

    public function update($id, $data)
    {
        $data = User::where('user_id', $id)->update($data);
        return $data;
    }


    /**
     * Get user by login key and passsword
     *
     * @param  string $loginKey
     * @param  string $mailAddress
     * @return mixed
     */

   
    public function isExists($id)
    {
        return $this->getInstance()::where('user_id', $id)->exists();
    }

}
