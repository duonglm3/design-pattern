<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
    use HasFactory;
    public $incrementing = false;
    protected $table = 'user_role';
    protected $primaryKey = ['user_id', 'role_id'];
    protected $fillable = self::FIELDS;
    
    public const FIELDS = [
        'user_id',
        'role_id',
        
    ];
}
