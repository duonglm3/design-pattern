<?php

namespace App\Http\Controllers;

use App\Http\Requests\Auth\LoginRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function viewLogin(Request $request)
    {
        return view('auth.login');
    }

    public function loginForm(LoginRequest $request)
    {
        $data = $request->only([
            'email',
            'password',
        ]);

        /*
         * Auth::attempt(['email', 'password'])
         * return false nếu login thất bại
         * return true nếu login thành công
         */
        $result = Auth::attempt($data);
        if ($result === false) {
            session()->flash('error', 'Thông tin đăng nhập sai hoặc tài khoản của bạn đã bị khóa !');

            return back();
        }

        return redirect()->route('users.index')->with('success', 'Đăng nhập thành công');
    }

    public function logout(Request $request)
    {
        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect()->route('home');
    }
}
