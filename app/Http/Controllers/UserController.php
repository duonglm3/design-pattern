<?php

namespace App\Http\Controllers;

use App\Http\Requests\Users\CreateRequest;
use App\Http\Requests\Users\UpdateRequest;
use App\Repositories\UserRoleRepository;
use App\Services\RoleService;
use App\Repositories\UserRepository;
use App\Services\UserRoleService;
use App\Services\UserService;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public $userService;
    public $roleService;
    public $userRoleService;
    public $userRoleRepository;
    public $userRepository;
    public function __construct(UserService $UserService, RoleService $RoleService, UserRoleService $UserRoleService, UserRoleRepository $UserRoleRepository, UserRepository $UserRepository)
    {
        $this->userService = $UserService;
        $this->roleService = $RoleService;
        $this->userRoleService = $UserRoleService;
        $this->userRoleRepository = $UserRoleRepository;
        $this->userRepository = $UserRepository;
    }

    public function index()
    {
        $this->authorize('CRUD_USERS');
        $data = $this->userService->get();
        return view('users.index', [
            'data' => $data,
        ]);
    }

    public function create()
    {
        $data = $this->roleService->get();
        return view('users.create', [
            'data' => $data,
        ]);
    }

    public function store(CreateRequest $request)
    {
        // dd($request->all());
        $params = request()->except('_token');
        $user = $this->userService->create($params);
        return redirect()->route('users.index')->with('success', 'Thêm mới thành công!');
    }

    public function edit($id)
    {
        $detail = $this->userRepository->getById($id);
        $dataRole = $this->roleService->get();
        $dataUserRole = $this->userRoleRepository->rolIds($id);
        return view('users.edit', compact('detail', 'dataRole', 'dataUserRole'));
    }

    public function update($id, UpdateRequest $request)
    {
        $params = request()->except('_token');
        $user = $this->userService->update($id, $params);
        return redirect()->route('users.index')->with('success', 'Cập nhật thành công!');
    }

    public function delete($id)
    {
        $this->userRepository->delete($id);
        return redirect()->route('users.index');
    }
}
