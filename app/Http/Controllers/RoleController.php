<?php

namespace App\Http\Controllers;

use App\Repositories\PermissionRepository;
use App\Repositories\PermissionRoleRepository;
use App\Repositories\RoleRepository;
use App\Services\PermisionService;
use App\Services\RoleService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RoleController extends Controller
{
    public $roleService;
    public $permisionService;
    public $roleRepo;
    public $permissionRepo;
    public $permissionRoleRepo;
    public function __construct(RoleService $RoleService, PermisionService $PermisionService, RoleRepository $RoleRepository, PermissionRepository $PermissionRepository, PermissionRoleRepository $PermissionRoleRepository)
    {
        $this->roleService = $RoleService;
        $this->permisionService = $PermisionService;
        $this->roleRepo = $RoleRepository;
        $this->permissionRepo = $PermissionRepository;
        $this->permissionRoleRepo = $PermissionRoleRepository;
    }
    public function index(){
        $this->authorize('CRUD_ROLES');
        $data = $this->roleService->get();
        return view('roles.index', compact('data'));
    }

    public function create(){
        $dataPermission = $this->permisionService->all();
        return view('roles.create', compact('dataPermission'));
    }

    public function store(Request $request){
        // dd($request->all());
        $params = request()->except('_token');
        $result = $this->roleService->create($params);
        return redirect()->route('roles.index')->with('success', 'Thêm mới thành công!');
    }

    public function edit($id){
        $data = $this->roleRepo->getById($id);
        $dataPermission = $this->permisionService->all();
        $datss = $this->permissionRoleRepo->permissionIds($id);
        return view('roles.edit', compact('data', 'dataPermission', 'datss'));
    }

    public function update($id, Request $request){
        $params = request()->except('_token');
        $role = $this->roleService->update($id, $params);
        return redirect()->route('roles.index')->with('success', 'Cập nhật thành công!');
    }

    public function delete($id)
    {
        $detail = $this->roleService->delete($id);
        return redirect()->route('roles.index');
    }
}
