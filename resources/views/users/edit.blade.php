@extends('layout')
@section('contents')
    <div class="container">
        <form action="{{ route('users.update', ['id' => $detail->id]) }}" method="POST">
            @csrf
            <div class="mt-5">
                <label for="">Name</label>
                <input class="form-control" type="text" name="name" value="{{ $detail->name }}">
                @error('name')
                    <span class="text-danger">{{ $message }}</span>
                @enderror
            </div>
            <div class="mt-5">
                <label for="">Email</label>
                <input class="form-control" type="email" name="email" value="{{ $detail->email }}">
                @error('email')
                    <span class="text-danger">{{ $message }}</span>
                @enderror
            </div>
            <div class="mt-5">
                <label for="">Chức vụ</label>
                <select name="code" class="custom-select" style="width:200px;">
                    @foreach ($dataRole as $row)
                        <option {{ in_array($row->id, $dataUserRole) ? 'selected' : '' }} value="{{ $row->id }}">{{ $row->code }}</option>
                    @endforeach
                </select>
                @error('code')
                    <span class="text-danger">{{ $message }}</span>
                @enderror
            </div>
            <button class="mt-3 btn btn-primary">Update</button>
        </form>
    </div>
@endsection
