@extends('layout')
@section('contents')
    <div class="container">
        <form action="{{ route('users.store') }}" method="POST">
            @csrf
            <div class="mt-5">
                <label for="">Name</label>
                <input class="form-control" type="text" name="name" value="{{ old('name') }}">
                @error('name')
                    <span class="text-danger">{{ $message }}</span>
                @enderror
            </div>
            <div class="mt-5">
                <label for="">Email</label>
                <input class="form-control" type="email" name="email" value="{{ old('email') }}">
                @error('email')
                    <span class="text-danger">{{ $message }}</span>
                @enderror
            </div>
            <div class="mt-5">
                <label for="">Password</label>
                <input class="form-control" type="password" name="password">
                @error('password')
                    <span class="text-danger">{{ $message }}</span>
                @enderror
            </div>
            <div class="mt-5">
                <label for="">Chức vụ</label>
                <select name="code" class="custom-select" style="width:200px;">
                    @foreach ($data as $row)
                        <option value="{{ $row->id }}">{{ $row->code }}</option>
                    @endforeach
                </select>
                @error('code')
                    <span class="text-danger">{{ $message }}</span>
                @enderror
            </div>
            <button class="mt-3 btn btn-primary">Create</button>
        </form>
    </div>
@endsection
