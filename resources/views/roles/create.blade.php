@extends('layout')
@section('contents')
    <div class="container">
        <form action="{{ route('roles.store') }}" method="POST">
            @csrf
            <div class="mt-5">
                <label for="">code</label>
                <input class="form-control" type="text" name="code" value="{{ old('code') }}">
                @error('code')
                    <span class="text-danger">{{ $message }}</span>
                @enderror
            </div>
            <div class="mt-5">
                <label for="">Quyền</label> <br>
                @foreach ($dataPermission as $row)
                    <input type="checkbox" value="{{ $row->id }}" name="permission[]">
                    <label for="">{{ $row->permission }}</label><br>
                @endforeach
                @error('code')
                    <span class="text-danger">{{ $message }}</span>
                @enderror
            </div>
            <button class="mt-3 btn btn-primary">Create</button>
        </form>
    </div>
@endsection
